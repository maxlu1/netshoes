import { Component, OnInit } from '@angular/core';
import { OrderCreate } from '../shared/orderCreate.service';

@Component({
  selector: 'app-topmenu',
  templateUrl: './topmenu.component.html',
  styleUrls: ['./topmenu.component.scss']
})
export class TopmenuComponent implements OnInit {

  constructor(
    public orderCreate: OrderCreate
  ) { }

  ngOnInit() {
   // console.log(this.orderCreate.showCartItens());
   this.orderCreate.countTotalCartItens();
  }

}
