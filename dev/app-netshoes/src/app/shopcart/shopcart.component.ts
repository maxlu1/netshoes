import { Component, OnInit, OnDestroy } from '@angular/core';
import { OrderCreate } from '../shared/orderCreate.service';
//
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-shopcart',
  templateUrl: './shopcart.component.html',
  styleUrls: ['./shopcart.component.scss']
})
export class ShopcartComponent implements OnInit, OnDestroy {
  public itemsAtCart;
  constructor(
    public orderCreate: OrderCreate
  ) { }


  ngOnInit() {
    this.itemsAtCart = this.orderCreate.showCartItens();
    //$(".showcase").css("width", "calc(100vw - 360px)")
    $(".showcase, body").addClass("showcase__fold");
    //console.log(this.orderCreate.showCartItens());
  }

  public changeCartItemQuantity(event: any, skuSize: string):void{
    //console.log( event.source.value +" / "+ skuSize);
    this.orderCreate.changeItemQuantity(skuSize, event.source.value);

  }

  public removeCartItem(skuSize: string){
    this.orderCreate.removeItem(skuSize);
  }

  ngOnDestroy(){
    $(".showcase, body").removeClass("showcase__fold");
  }



}
