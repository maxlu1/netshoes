import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopcartComponent } from './shopcart/shopcart.component'

const routes: Routes = [
  { path: 'cart', component: ShopcartComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
