import { Product } from './product.model'
import { OrderItem } from './orderItem.model'

export class OrderCreate{
    public cartItens: OrderItem[] = this.recoveryMyCartItems();
    public sumCartItems: number = 0;
    public showCartItens():OrderItem[]{
        return this.cartItens;
    }

    public recoveryMyCartItems():Array<any>{
        let myrecoveredItems: [] = []
        if(localStorage.myCartItems !== undefined){
            myrecoveredItems = JSON.parse(localStorage.myCartItems)
        }
        return myrecoveredItems
    }

    public addNewItem(product:Product, size:string): void{
        //console.log(oferta);
        let cartNewItem: OrderItem = new OrderItem(
            product.id,
            product.sku,
            size,
            product.sku+"__"+size,
            product.title,
            product.price,
            1,
        )
        let sumThisItem = this.cartItens.find((item: OrderItem) => item.skuAndSize === cartNewItem.skuAndSize);
        if(sumThisItem){
            sumThisItem.quantity ++;
        }else{
            this.cartItens.unshift(cartNewItem);
        }        
        localStorage.setItem("myCartItems", JSON.stringify(this.cartItens));
        this.countTotalCartItens(); 
        //console.log(this.showCartItens());
    }

    public changeItemQuantity(skuAndSize: string, newQuantity: number):void{
        let changeItem = this.cartItens.find((item: OrderItem) => item.skuAndSize === skuAndSize);        
        if(changeItem){
            changeItem.quantity = newQuantity;
        }
        localStorage.setItem("myCartItems", JSON.stringify(this.cartItens));
        this.countTotalCartItens(); 
    }

    public removeItem(skuAndSize: string):void{
        let removeItem = this.cartItens.find((item: OrderItem) => item.skuAndSize === skuAndSize);
        if(removeItem){
            this.cartItens.splice(this.cartItens.indexOf(removeItem), 1);
        }
        localStorage.setItem("myCartItems", JSON.stringify(this.cartItens));
        this.countTotalCartItens();       
    }

    public countTotalCartItens():void{
        let i, sum;
        sum = 0;
        for (i = 0; i < this.cartItens.length; i++) {
            sum += this.cartItens[i].quantity
        }
        this.sumCartItems = sum;

    }
}