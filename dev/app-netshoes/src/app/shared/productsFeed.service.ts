import { Http, Headers, Response } from '@angular/http'
import { Injectable } from '@angular/core'
import { ApiDataURL } from './data.api'
import { Product } from './product.model'

@Injectable()
export class ProductsFeed{
    constructor(private http: Http){}

    public getAllProducts(): Promise<Product[]> {
        return this.http.get(`${ApiDataURL}/products`)
            .toPromise()
            .then((response: Response) => response.json())
    }
    
    public getProductsWithFreeShipping(): Promise<Product[]> {
        return this.http.get(`${ApiDataURL}/products?isFreeShipping=true`)
            .toPromise()
            .then((response: Response) => response.json())
    }     
    
    public getProductsWitOuthFreeShipping(): Promise<Product[]> {
        return this.http.get(`${ApiDataURL}/products?isFreeShipping=false`)
            .toPromise()
            .then((response: Response) => response.json())
    }      



}