class OrderItem {
    constructor(
        public id: number,
        public sku: string,
        public size: string,
        // Key to select items with different size options:
        public skuAndSize: string,
        public title: string,
        public price: number,
        public quantity: number,
        
    ) { }
}

export { OrderItem }