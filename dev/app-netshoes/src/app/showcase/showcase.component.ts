import { Component, OnInit } from '@angular/core';
import { ProductsFeed } from '../shared/productsFeed.service';
import { OrderCreate } from '../shared/orderCreate.service';
import { Router } from '@angular/router';
import { Product } from '../shared/product.model'
//
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.scss'],
  providers: [ProductsFeed]
})
export class ShowcaseComponent implements OnInit {
  public goodsList;
  // 
  // 
  constructor(
    private productsFeed: ProductsFeed,
    private orderCreate: OrderCreate,
    public router: Router
  ) { }

  ngOnInit() {
    this.productsFeed.getAllProducts().then(( response: any ) => { 
      this.goodsList = response/*.sort(function(a, b){return 16 - Math.random()});*/
    })
    .catch(( param: any ) => { 
      console.log(param);
    })   
    

  }

  public chooseASize(sku:string):void{
    $("#shwc__"+sku).fadeIn();
  }

  public buyThisItem(id:number, size:string):void{
    this.orderCreate.addNewItem(this.goodsList[id], size);
    this.router.navigate(['cart']);
  }

}
