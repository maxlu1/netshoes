import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { ShowcaseComponent } from './showcase/showcase.component';
import { ShopcartComponent } from './shopcart/shopcart.component';
import { TopmenuComponent } from './topmenu/topmenu.component';
import { HashLocationStrategy, LocationStrategy} from '@angular/common'

// Services
import { OrderCreate } from './shared/orderCreate.service'

@NgModule({
  declarations: [
    AppComponent,
    ShowcaseComponent,
    ShopcartComponent,
    TopmenuComponent
  ],
  imports: [
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule
  ],
  providers: [ OrderCreate, {provide: LocationStrategy, useClass: HashLocationStrategy} ],
  bootstrap: [AppComponent]
})
export class AppModule { }
