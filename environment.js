const jsonServer = require('json-server'),
server = jsonServer.create(),
router = jsonServer.router('./data/products.json'),
middlewares = jsonServer.defaults(),
browserSync = require('browser-sync').create();
//Auto exec servers to front-end APP and API data
;(function(){
	server.use(middlewares)
	server.use(router)
	server.listen(3000, () => {
	browserSync.init({
		  server: "./dist",
		  port: 4000
		});	
	})
}())
