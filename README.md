# Accomplishment - Netshoes Cart Test: by. Max Salvado (max.lu1.com.br)

## 1. Features 

* This project was made with frame-work JavaScript Angular.js;
* The API to manage database, are automatically mounted by environment server, using Node.js services packages;
* Like test dependencies, this project use modules Jasmine and Karma.
* The front-end app view interface, are managed by Angular Material package.


## 2. On stage: running the product build

* Using your CLI terminal (bash, shell, prompt, etc.), access the directory `/netshoes/`;
* At location, run the follow command install the necessary Node.js packages: [ npm install ];
* TIP: if this action returns some error, try use [ sudo ] like prefix of the last command to avoid system admin profile issues;
* After install the packages, the environment are ready to run the servers, for this run the command [ node environment.js ];
* At moment we have two servers working: listening on `localhost:3000`, is the API server to manager database and another, on `localhost:4000` we have the front-end app.

## 3. Back stage: accessing developer area
* Using your CLI terminal (bash, shell, prompt, etc.), access the directory `/netshoes/dev/app-netshoes/`;
* At location, run the follow command install the necessary Node.js packages: [ npm install ];
* TIP: if this action returns some error, try use [ sudo ] like prefix of the last command to avoid system admin profile issues;
* After install the packages the frame-work Angular.js are ready to run - at location run this command to start a server project [ ng serve ];
* REMEMBER: is very important to keep the environment servers (topic #2) running, because the data to feed front-end app, are provided by them.
* TIP: You can use any coder software to browse the projet files, but is strongly recommended that you use Visual Studio Code - see more at: https://code.visualstudio.com/

## 3.1 Back stage: creating a new build product
* If you changes something at developer project or just wanna get a new fresh build to app, use your CLI terminal (bash, shell, prompt, etc.), and access the directory `/netshoes/dev/app-netshoes/`;
* At location run the command [ ng build --prod ];
* On finish the process, the new build will be available at directory `/netshoes/dist` and ready to requests from environment server described at topic #2;
* REMEMBER: while this task are running, the app server will be unavailable - but it will returns when the build task done.

## 4. Try-out: delivery unit tests
* Using your coder software (recommended: Visual Studio Code - see more at: https://code.visualstudio.com/), access the directory `/netshoes/dev/app-netshoes/`;
* At tree files look for the `/src/karma.conf.js` to access and changes the configurations to Karma;
* By default, Angular.js use the file `/src/test.ts` like entry point for the tests;
* To create new tests, use the files with suffix ".spec.ts" presents under the all component and modules directory from files tree.



## Conclusion

* Thanks a lot for dedicate your time to this project!
* If you have some kind of question or issue, feel free to email me: max.salvado@lu1.com.br. Cheers!
